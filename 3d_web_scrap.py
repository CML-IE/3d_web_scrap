from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import pandas as pd
import time

driver = webdriver.Firefox(executable_path=r"C:\Python37\BrowsersDriver\geckodriver.exe")

number_of_pages = 100

timeout = 15

website_source = []
models_extensions = []
models_names = []
models_links = []
models_downloads = []
models_likes = []
models_comments = []
models_tags = []
models_pictures = []
models_dates = []
models_tags = []

for l in range(number_of_pages):
    url = "https://grabcad.com/library?page="+ str(l+1) + "&per_page=100&time=all_time&sort=popular"
    driver.get(url)
    try:
        WebDriverWait(driver, timeout).until(EC.visibility_of_all_elements_located((By.CLASS_NAME, 'softwares')))
        extensions = driver.find_elements_by_class_name('softwares')
        names = driver.find_elements_by_xpath('//span[@class="name ng-binding"]')
        urls = driver.find_elements_by_xpath('//a[@class="modelLink"]')
        likes = driver.find_elements_by_xpath('//span[@ng-if="!model.has_liked"]/following-sibling::*[1]')
        downloads = driver.find_elements_by_xpath('//span[@class="gc-icon gc-icon-download"]/following-sibling::*[1]')
        comments = driver.find_elements_by_xpath('//span[@class="gc-icon gc-icon-comment"]/following-sibling::*[1]')
        creation_date = driver.find_elements_by_xpath('//a[@class="modelCreated"]')
        models_links += [ext.get_attribute('href') for ext in urls]
        models_extensions += [ext.text for ext in extensions]
        models_names += [name.text for name in names]
        models_likes += [like.text for like in likes]
        models_downloads += [dl.text for dl in downloads]
        models_comments += [comment.text for comment in comments]

    except TimeoutException:
        print("Loading took too long for page: ", l+1)
        n = 0
        if n>3:
            print("Trying again")
            l -= 1
            n += 1
        else:
            warning = "We tried page {l+1} too many times. We move onto the next."
            print(warning)


soft_lists = []
uniques = []

for l in models_extensions:
    soft = l.split(", ")
    uniques.extend(soft)
    soft_lists.append(soft)

softwares = pd.DataFrame(soft_lists)

uni = pd.DataFrame(uniques)
g = uni.apply(pd.value_counts)

columns = pd.DataFrame(uniques).drop_duplicates()


column_names = ["ModelName", "SoftwareExtensions","ModelURL", "NumberDownloads",
                "NumberLikes", "DownloadURL", "TimeMarkScrap"]

# =============================================================================
# column_names = ["Website", "ModelName", "SoftwareExtensions","ModelURL",
#                 "ModelPictureURL", "UploadDate", "NumberDownloads",
#                 "NumberLikes", "Categories", "Tags", "DownloadURL",
#                 "TimeMarkScrap"]
# =============================================================================
